﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using EnvDTE80;

namespace LastShotStudiosInc.PhysicalSolutionFolders
{
    /// <summary>
    /// Interaction logic for EditFolderWindow.xaml
    /// </summary>
    public partial class EditFolderWindow : Window, IDisposable
    {
        public FolderStorageEntry entry;
        public bool submitted;
        private EnvDTE80.DTE2 dte;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;

        /// <summary>
        /// Creates a relative path from one file or folder to another.
        /// </summary>
        /// <param name="fromPath">Contains the directory that defines the start of the relative path.</param>
        /// <param name="toPath">Contains the path that defines the endpoint of the relative path.</param>
        /// <returns>The relative path from the start directory to the end path.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="UriFormatException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public static String MakeRelativePath(String fromPath, String toPath)
        {
            if (String.IsNullOrEmpty(fromPath)) throw new ArgumentNullException("fromPath");
            if (String.IsNullOrEmpty(toPath)) throw new ArgumentNullException("toPath");

            Uri fromUri = new Uri(fromPath);
            Uri toUri = new Uri(toPath);

            if (fromUri.Scheme != toUri.Scheme) { return toPath; } // path can't be made relative.

            Uri relativeUri = fromUri.MakeRelativeUri(toUri);
            String relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            if (toUri.Scheme.ToUpperInvariant() == "FILE")
            {
                relativePath = relativePath.Replace(
                    System.IO.Path.AltDirectorySeparatorChar,
                    System.IO.Path.DirectorySeparatorChar);
            }

            return relativePath;
        }

        public EditFolderWindow()
        {
            InitializeComponent();
            folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            dte = PhysicalSolutionFolders.GetDTE();
        }

        public bool BringUp(FolderStorageEntry entry = new FolderStorageEntry())
        {
            // Can't default construct struct so this is a workaround...
            if (entry.IsDefault()) entry.Initialize();

            if (entry.relative && !String.IsNullOrEmpty(entry.physicalFolder))
            {
                entry.physicalFolder = System.IO.Path.GetFullPath(
                    System.IO.Path.Combine(
                        PhysicalSolutionFolders.
                            GetSolutionDirectory(dte),
                            entry.physicalFolder
                    )
                );
            }

            submitted = false;
            this.entry = entry;

            SolutionTextBox.Text = entry.solutionFolder;
            PhysicalTextBox.Text = entry.physicalFolder;
            FilterTextBox.Text = entry.filter;
            DirectoryFilterTextBox.Text = entry.dirFilter;
            RelativeCheckBox.IsChecked = entry.relative;
            RecursiveCheckBox.IsChecked = entry.recursive;
            ForceCheckBox.IsChecked = entry.force;
            DropEmptyCheckBox.IsChecked = entry.dropEmpty;

            ShowDialog();
            return submitted;
        }

        private void doSubmit()
        {
            bool valid = doValidateSolution();
            valid = doValidatePhysical() && valid;
            if (!valid) return;

            entry = new FolderStorageEntry();
            entry.relative = RelativeCheckBox.IsChecked.GetValueOrDefault(false);
            entry.recursive = RecursiveCheckBox.IsChecked.GetValueOrDefault(false);
            entry.force = ForceCheckBox.IsChecked.GetValueOrDefault(false);
            entry.dropEmpty = DropEmptyCheckBox.IsChecked.GetValueOrDefault(false);
            entry.solutionFolder = SolutionTextBox.Text;
            entry.physicalFolder = PhysicalTextBox.Text;
            entry.filter = FilterTextBox.Text;
            entry.dirFilter = DirectoryFilterTextBox.Text;
            if (entry.relative && System.IO.Path.IsPathRooted(entry.physicalFolder))
            {
                String solutionDirectory = PhysicalSolutionFolders.GetSolutionDirectory(dte);
                entry.physicalFolder = MakeRelativePath(
                    solutionDirectory, entry.physicalFolder);
                if (entry.physicalFolder == solutionDirectory)
                {
                    MessageBox.Show("Could not convert physical path to solution relative!");
                    return;
                }
            }
            submitted = true;
            Hide();
        }

        private void doCancel()
        {
            Hide();
        }

        private bool doValidateSolution()
        {
            String val = SolutionTextBox.Text;
            SolutionInvalid.Visibility = System.Windows.Visibility.Visible;

            if (val == "/") return false;
            if (val.Length == 0) return false;
            if (Regex.Match(val, "[?:&\\*\"<>|#%]|[/][/]").Success) return false;

            String[] words = {
                "CON", "AUX", "PRN", "COM1", "LPT2", "[.]{1,2}"
            };
            foreach (String word in words)
            {
                if (Regex.Match(val, "(^|[/])" + word + "($|[/])").Success) return false;
            }

            if (val[0] == '/') val = val.Substring(1);
            if (val[val.Length - 1] == '/') val = val.Substring(0, val.Length - 1);
            SolutionTextBox.Text = val;

            SolutionInvalid.Visibility = System.Windows.Visibility.Hidden;
            return true;
        }

        private bool doValidatePhysical()
        {
            if (!Directory.Exists(PhysicalTextBox.Text))
            {
                PhysicalInvalid.Visibility = System.Windows.Visibility.Visible;
                return false;
            }

            PhysicalInvalid.Visibility = System.Windows.Visibility.Hidden;
            return true;
        }

        private void doNavigateFolder()
        {
            String solutionDir = PhysicalSolutionFolders.GetSolutionDirectory(dte);
            String p = PhysicalTextBox.Text;
            if (p.Length == 0 || !Directory.Exists(p))
                p = solutionDir;
            folderBrowser.SelectedPath = p;
            System.Windows.Forms.DialogResult result = folderBrowser.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                PhysicalTextBox.Text = folderBrowser.SelectedPath;
            }

            doValidatePhysical();
        }

        private void btnSubmit(object sender, RoutedEventArgs e)
        {
            doSubmit();
        }

        private void btnCancel(object sender, RoutedEventArgs e)
        {
            doCancel();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    doCancel();
                    break;
                case Key.Enter:
                    doSubmit();
                    break;
            }
        }

        private void txtSolution_LostFocus(object sender, RoutedEventArgs e)
        {
            doValidateSolution();
        }

        private void btnNav_Click(object sender, RoutedEventArgs e)
        {
            doNavigateFolder();
        }

        private void dockRelative_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Handled) return;
            RelativeCheckBox.IsChecked = !RelativeCheckBox.IsChecked.GetValueOrDefault(false);
        }

        private void dockForce_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Handled) return;
            ForceCheckBox.IsChecked = !ForceCheckBox.IsChecked.GetValueOrDefault(false);
        }

        private void txtPhysical_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    doNavigateFolder();
                    break;
            }
        }

        private void grpSubdirectories_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Handled) return;
            RecursiveCheckBox.IsChecked = !RecursiveCheckBox.IsChecked.GetValueOrDefault(false);
        }

        private void cbRecursive_Checked(object sender, RoutedEventArgs e)
        {
            DirectoryFilterTextBox.IsEnabled = true;
        }

        private void cbRecursive_Unchecked(object sender, RoutedEventArgs e)
        {
            DirectoryFilterTextBox.IsEnabled = false;
        }

        private void dockDropEmpty_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Handled) return;
            DropEmptyCheckBox.IsChecked = !DropEmptyCheckBox.IsChecked.GetValueOrDefault(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposeManagedAndNative)
        {
            if (disposeManagedAndNative)
                folderBrowser.Dispose();
        }
    }
}
