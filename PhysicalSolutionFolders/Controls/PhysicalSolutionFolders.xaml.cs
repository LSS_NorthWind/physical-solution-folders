﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EnvDTE80;
using EnvDTE;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace LastShotStudiosInc.PhysicalSolutionFolders
{
    /// <summary>
    /// Interaction logic for PhysicalSolutionFolders.xaml
    /// </summary>
    public partial class PhysicalSolutionFolders :
        UserControl, IVsSolutionEvents, IDisposable
    {
        private struct FolderRow
        {
            public uint token;
            public FolderStorageEntry entry;
            public RowDefinition row;
            public TextBlock solution;
            public TextBlock physical;
            public List<FrameworkElement> uiObjects;
        }
        private List<FolderRow> folderRows;
        private List<Border> lineBackgrounds;
        private EditFolderWindow editFolderWindow;
        private int numCols;
        private EnvDTE80.DTE2 dte;
        private uint svCookie;
        private IVsStatusbar statusBar;

        private IFolderStorage folderStorage;
        public IFolderStorage FolderStorage
        {
            set
            {
                folderStorage = value;
                folderStorage.AfterReloaded += folderStorage_AfterReloaded;
            }
        }

        public PhysicalSolutionFolders()
        {
            folderRows = new List<FolderRow>();
            lineBackgrounds = new List<Border>();
            editFolderWindow = new EditFolderWindow();
            InitializeComponent();
        }

        private void ucPhysicalSolutionFolders_Initialized(object sender, EventArgs e)
        {
            numCols = FoldersGrid.ColumnDefinitions.Count;

            dte = PhysicalSolutionFolders.GetDTE();
            IVsSolution sln = (IVsSolution)Package.GetGlobalService(typeof(SVsSolution));
            sln.AdviseSolutionEvents(this, out svCookie);
            statusBar = (IVsStatusbar)Package.GetGlobalService(typeof(SVsStatusbar));
        }

        private void ucPhysicalSolutionFolders_Loaded(object sender, RoutedEventArgs e)
        {
            IsEnabled = dte.Solution.IsOpen;
            SynchronizeStatus();
        }

        public int OnAfterOpenSolution(object pUnkReserved, int fNewSolution)
        {
            IsEnabled = true;
            SynchronizeStatus();

            return VSConstants.S_OK;
        }

        public int OnAfterCloseSolution(object pUnkReserved)
        {
            IsEnabled = false;
            doClearRows();
            SynchronizeStatus();
            return VSConstants.S_OK;
        }

        private void doClearRows()
        {
            // Remove all rows, reverse order to avoid spurious grid row adjustments
            while (folderRows.Count > 0)
                doRemoveRow(folderRows.Count - 1);
        }

        void folderStorage_AfterReloaded(object sender_, EventArgs e)
        {
            IFolderStorage sender = (IFolderStorage)sender_;
            doClearRows();

            // Add rows
            int numEntries = sender.Count;
            for (int i = 0; i < numEntries; i++)
            {
                uint token = sender.GetToken(i);
                doAddRow(token, sender.Get(token));
            }
        }
        public void CodeExample(DTE2 dte)
        {
            try
            {   // Open a project before running this sample
                Projects prjs = dte.Solution.Projects;
                string msg = "There are " + prjs.Count.ToString() + " projects in this collection.";
                msg += "\nThe application containing this Projects collection: " + prjs.DTE.Name;
                msg += "\nThe parent object of the Projects collection: " + prjs.Parent.Name;
                msg += "\nThe GUID representing the Projects type: " + prjs.Kind;
                if (prjs.Properties != null)
                {
                    msg += "\nProperties:";
                    foreach (Property prop in prjs.Properties)
                    {
                        msg += "\n   " + prop.Name;
                    }
                }
                MessageBox.Show(msg, "Projects Collection");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void doEditRow(uint token)
        {
            for (int i = 0; i < folderRows.Count; i++)
            {
                FolderRow f = folderRows[i];
                if (f.token != token) continue;

                if (editFolderWindow.BringUp(f.entry))
                {
                    f.entry = editFolderWindow.entry;
                    folderStorage.Update(f.token, f.entry);
                    f.solution.Text = f.entry.solutionFolder;
                    f.physical.Text = f.entry.physicalFolder;
                    folderRows[i] = f;

                    statusBar.SetText("Updated physical folders entry for solution folder " +
                        f.entry.solutionFolder);
                }
                else
                {
                    statusBar.SetText("");
                }
                break;
            }
        }

        void lblFolder_MouseDown(object sender_, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                FrameworkElement sender = (FrameworkElement)sender_;
                doEditRow((uint)sender.Tag);
            }
        }

        private void btnEditRow_Click(object sender_, RoutedEventArgs e)
        {
            FrameworkElement sender = (FrameworkElement)sender_;
            doEditRow((uint)sender.Tag);
        }

        private ImageBrush getResourceImage(String s)
        {
            return new ImageBrush(
                new System.Windows.Media.Imaging.BitmapImage(new Uri("pack://application:,,,/PhysicalSolutionFolders;component/resources/" +
                    s))
            );
        }
        
        private void btnSynchronizeAll_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < folderRows.Count; i++)
            {
                if (!doSynchronizeRow(i))
                {
                    statusBar.SetText("Synchronization Error: " +
                        "Row  " + (i + 1).ToString() + " of " +
                        folderRows.Count.ToString() + " failed to synchronize. Aborted.");
                    return;
                }
            }

            statusBar.SetText("Synchronization complete. " +
                "Synchronized " + folderRows.Count.ToString() + " rows.");
            return;
        }

        private static void ShowSynchErrorMessage(
            String itemName, int pathIdx, FolderStorageEntry entry)
        {
            String msg = "Synchronization Error:" +
                "\nEncountered non-solution folder item [" + itemName + "]" +
                " on path segment index " + pathIdx.ToString() +
                " on solution path " + entry.solutionFolder +
                ".\nDelete this item and try again.";
            MessageBox.Show(msg);
        }

        /**
         * Notes:
         * Can cascade solution folders by casting Project.Object -> SolutionFolder
         * Can turn project item into solution folder by ProjectItem.SubProject.Object -> SolutionFolder
         *      If -> SolutionFolder fails then it's real project
         *      If SubProject returns null then it's simple item
         * Can add items (incl. projects) to solution folder via Project.ProjectItems.AddFromFile()
         * Can add solution folders to solution folder via Project.Object -> SolutionFolder .AddSolutionFolder
         */
        private static Project CreateOrClear(
            string[] path, int idx, Project parent, FolderStorageEntry entry)
        {
            // If parent is end of the line, return it
            if (idx >= path.Length)
            {
                return parent;
            }

            String name = path[idx];
        
            // 1 indexed...
            ProjectItems items = parent.ProjectItems;
            ProjectItem item = null;
            Project prj = null;
            int i;
            for (i = 1; i <= items.Count &&
                (item = items.Item(i)).Name != name; i++) ;

            SolutionFolder s = (SolutionFolder)parent.Object;
        
            // If not found, create and move on
            if (i <= items.Count)
            {
                // Detect if this is a simple item...
                if ((prj = item.SubProject) == null)
                {
                    if (entry.force)
                    {
                        item.Delete();
                        prj = null;
                    }
                    else
                    {
                        ShowSynchErrorMessage(item.Name, idx, entry);
                        return null;
                    }
                }
                else
                {
                    // Detect if this is a solution folder
                    SolutionFolder temp = null;
                    try
                    {
                        temp = (SolutionFolder)prj.Object;
                    }
                    catch (InvalidCastException)
                    { }

                    if (temp == null)
                    {
                        if (entry.force)
                        {
                            item.Delete();
                            prj = null;
                        }
                        else
                        {
                            ShowSynchErrorMessage(item.Name, idx, entry);
                            return null;
                        }
                    }
                }
            }

            // If prj still empty, create it
            if (prj == null)
            {
                prj = s.AddSolutionFolder(name);
            }

            //Descend...
            return CreateOrClear(path, idx + 1, prj, entry);
        }

        private static Project CreateOrClear(
            string[] path, int idx, Solution2 parent, FolderStorageEntry entry)
        {
            if (idx >= path.Length) return null;
            String name = path[idx];
            Project item = null;
        
            // Note: 1-indexed....
            int i;
            for (i = 1; i <= parent.Count &&
                (item = parent.Item(i)).Name != name; i++) ;
        
            // If not found, create and move on
            if (i > parent.Count)
            {
                item = parent.AddSolutionFolder(name);
            }

            // Otherwise, ensure this is a solution folder
            else
            {
                SolutionFolder temp = null;
                try
                {
                    temp = (SolutionFolder)item.Object;
                }
                catch (InvalidCastException)
                {}

                if (temp == null)
                {
                    // If forcing, delete item and recreate it
                    if (entry.force)
                    {
                        item.Delete();
                        item = parent.AddSolutionFolder(name);
                    }

                    // Otherwise, throw up a message and stop
                    else
                    {
                        ShowSynchErrorMessage(item.Name, idx, entry);
                        return null;
                    }
                }
            }

            // Pass it to project CreateOrDelete
            return CreateOrClear(path, idx + 1, item, entry);
        }

        private struct FileEntry
        {
            public String name, fullPath;

            public FileEntry(String name_, String fullPath_)
            {
                name = name_;
                fullPath = fullPath_;
            }
        }

        private struct FolderEntry
        {
            public String name, fullPath;
            public List<FileEntry> files;
            public List<FolderEntry> folders;
            public uint totalChildElements;

            public FolderEntry(String name_, String fullPath_)
            {
                name = name_;
                fullPath = fullPath_;
                files = new List<FileEntry>();
                folders = new List<FolderEntry>();
                totalChildElements = 0;
            }

            public uint CountTotalChildElements()
            {
                totalChildElements = (uint)files.Count;
                foreach (FolderEntry entry in folders)
                {
                    totalChildElements += entry.CountTotalChildElements() +1;
                }

                return totalChildElements;
            }
        }

        private FolderEntry CollectTargets(
            String targetAbs, FolderStorageEntry entry,
            Regex rFile = null, Regex rDir = null)
        {
            FolderEntry folder = new FolderEntry(
                System.IO.Path.GetFileName(targetAbs),
                targetAbs);

            string[] files = System.IO.Directory.GetFiles(targetAbs);
            if (rFile == null)
            {
                rFile = new Regex(entry.filter);
            }
            foreach (string file in files)
            {
                String name = System.IO.Path.GetFileName(file);

                if (rFile.Match(name).Success)
                {
                    folder.files.Add(new FileEntry(name, file));
                }
            }

            if (entry.recursive)
            {
                if (rDir == null)
                {
                    rDir = new Regex(entry.dirFilter);
                }

                string[] directories = System.IO.Directory.GetDirectories(targetAbs);
                foreach (string dir in directories)
                {
                    String name = System.IO.Path.GetFileName(dir);
                    if (rDir.Match(name).Success)
                    {
                        FolderEntry f = CollectTargets(dir, entry, rFile, rDir);

                        // Does client want empty directories trimmed?
                        if (!entry.dropEmpty || f.files.Count > 0 || f.folders.Count > 0)
                        {
                            folder.folders.Add(f);
                        }
                    }
                }
            }

            return folder;
        }

        private struct ProgressInfo
        {
            public uint pdwCookie;
            public uint itemsComplete;
            public uint totalItems;
            public String label;
            public bool track;
        }

        private void SyncrhonizeFolder(
            Project prj, FolderEntry target, FolderStorageEntry entry,
            ref ProgressInfo progress)
        {
            if (prj == null)
                throw new ArgumentNullException("Solution folder is null");

            // Associate names of files with files and folders with folders
            // Real projects count as files since they have file backing
            Dictionary<String, ProjectItem>
                folderFiles = new Dictionary<string,ProjectItem>(),
                folderDirectories = new Dictionary<string,ProjectItem>();

            for (int i = 1; i <= prj.ProjectItems.Count; i++)
            {
                ProjectItem item = prj.ProjectItems.Item(i);
                Project sub = item.SubProject;
                Dictionary<String, ProjectItem> dest = folderFiles;
                if (item.SubProject != null)
                {
                    try
                    {
                        SolutionFolder s = (SolutionFolder)sub.Object;
                        dest = folderDirectories;
                    }
                    catch (InvalidCastException)
                    {}
                }

                String name = item.Name;

                // If item with same name already in collection, remove this one
                // If item has multiple file names, we don't support that either and should remove...
                if (item.FileCount > 1 || dest.ContainsKey(name))
                {
                    item.Delete();
                    i--;
                }
                else
                {
                    dest.Add(name, item);
                }
            }

            foreach (FileEntry file in target.files)
            {
                bool doAdd = true;

                // Does file already exist in folder and match this file?
                if (folderFiles.ContainsKey(file.name))
                {
                    ProjectItem item = folderFiles[file.name];
                    if (item.FileNames[1] == file.fullPath)
                    {
                        doAdd = false;
                    }
                    else
                    {
                        item.Delete();
                    }

                    folderFiles.Remove(file.name);
                }

                if (doAdd)
                    prj.ProjectItems.AddFromFile(file.fullPath);

                if (progress.track)
                {
                    progress.itemsComplete++;
                    statusBar.Progress(
                        progress.pdwCookie, 1, progress.label, progress.itemsComplete, progress.totalItems);
                }
            }

            if (entry.recursive)
            {
                SolutionFolder s = (SolutionFolder)prj.Object;
                foreach (FolderEntry dir in target.folders)
                {
                    bool doAdd = true;
                    ProjectItem item = null;

                    // Do we already have this directory?
                    if (folderDirectories.ContainsKey(dir.name))
                    {
                        // SolutionFolders don't have physical paths so we just don't add
                        doAdd = false;
                        item = folderDirectories[dir.name];
                        folderDirectories.Remove(dir.name);
                    }

                    Project p = null;
                    if (doAdd)
                        p = s.AddSolutionFolder(dir.name);
                    else
                        p = item.SubProject;

                    SyncrhonizeFolder(p, dir, entry, ref progress);

                    if (progress.track)
                    {
                        progress.itemsComplete++;
                        statusBar.Progress(
                            progress.pdwCookie, 1, progress.label, progress.itemsComplete, progress.totalItems);
                    }
                }
            }

            // Clean up files and directories that were not synched
            foreach (Dictionary<String, ProjectItem> leftover in
                new Dictionary<String, ProjectItem>[]{
                    folderFiles, folderDirectories
                })
            {
                foreach (KeyValuePair<String, ProjectItem> pair in leftover)
                {
                    pair.Value.Delete();
                }
            }
        }

        private bool doSynchronizeRow(int idx)
        {
            if (idx < 0 || idx > folderRows.Count)
                throw new IndexOutOfRangeException();

            FolderStorageEntry entry = folderRows[idx].entry;

            // Ensure that folder exists on disk, otherwise message and abort
            String folderPath = entry.GetPhysicalFolderAbsolute(dte);
            if (!System.IO.Directory.Exists(folderPath))
            {
                statusBar.SetText("Synchronization Error:" +
                     " Could not synchronize solution folder " + entry.solutionFolder + ","
                    + " physical folder " + folderPath + " does not exist.");
                return false;
            }

            // Parse solution folder path
            string[] sfParts = entry.solutionFolder.Split(new char[]{'/'});

            if (sfParts.Length < 1)
            {
                statusBar.SetText("Synchronization Error:" +
                    " Could not synchronize solution folder " + entry.solutionFolder + "," +
                    " solution folder path was empty.");
                return false;
            }

            // Prep target solution folder
            Solution2 sln = (Solution2)dte.Solution;
            Project target = CreateOrClear(sfParts, 0, sln, entry);

            // If target is null, an error was encountered
            if (target == null)
            {
                statusBar.SetText("Synchronization Error:" +
                    " Could not synchronize solution folder " + entry.solutionFolder +
                    ", non-solution-folder items were encountered.");
                return false;
            }

            FolderEntry targetFolder = CollectTargets(folderPath, entry);

            // Otherwise, replicate on disk folder structure to folder
            ProgressInfo progress;
            progress.label = "Synchronizing row " + (idx + 1);
            progress.totalItems = targetFolder.CountTotalChildElements();
            progress.itemsComplete = 1;
            progress.pdwCookie = 0;
            progress.track = (VSConstants.S_OK == statusBar.Progress(
                ref progress.pdwCookie, 1, "", 0, 0));
            SyncrhonizeFolder(target, targetFolder, entry, ref progress);
            if (progress.track)
                statusBar.Progress(ref progress.pdwCookie, 0, "", 0, 0);
            statusBar.SetText("Synchronized solution folder " +
                entry.solutionFolder + " to " + folderPath);
            return true;
        }

        private int getRowIndexForToken(uint token)
        {
            for (int i = 0; i < folderRows.Count; i++)
            {
                if (folderRows[i].token == token)
                    return i;
            }

            throw new KeyNotFoundException();
        }

        private void doRemoveRow(int index)
        {
            if (index < 0 || index >= folderRows.Count)
                throw new IndexOutOfRangeException();

            FolderRow row = folderRows[index];
            foreach (FrameworkElement elem in row.uiObjects)
            {
                FoldersGrid.Children.Remove(elem);
            }
            FoldersGrid.RowDefinitions.Remove(row.row);
            folderRows.Remove(row);

            for (; index < folderRows.Count; index++)
            {
                row = folderRows[index];
                foreach (FrameworkElement elem in row.uiObjects)
                {
                    Grid.SetRow(elem, Grid.GetRow(elem) - 1);
                }
            }

            SynchronizeLineBackgrounds();
            SynchronizeStatus();
        }

        private void btnRemoveRow_Click(object sender_, RoutedEventArgs e)
        {

            FrameworkElement sender = (FrameworkElement)sender_;
            uint token = (uint)sender.Tag;
            int idx = getRowIndexForToken(token);
            String solutionFolder = folderRows[idx].entry.solutionFolder;
            doRemoveRow(idx);
            folderStorage.Remove(token);
            statusBar.SetText("Removed entry for physical solution folder " +
                solutionFolder);
        }

        private void doAddRow(uint token, FolderStorageEntry entry)
        {
            FolderRow f = new FolderRow();
            f.uiObjects = new List<FrameworkElement>();

            f.entry = entry;

            f.row = new RowDefinition();
            f.row.Height = new GridLength(30);
            int rowIdx = FoldersGrid.RowDefinitions.Count;
            FoldersGrid.RowDefinitions.Add(f.row);

            Label l = new Label();
            TextBlock tb = new TextBlock();
            tb.Text = f.entry.solutionFolder;
            l.Content = tb;
            l.Foreground = Brushes.LightGray;
            l.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Grid.SetRow(l, rowIdx);
            Grid.SetColumn(l, 0);
            l.MouseDown += lblFolder_MouseDown;
            f.uiObjects.Add(l);
            f.solution = tb;

            l = new Label();
            tb = new TextBlock();
            tb.Text = f.entry.physicalFolder;
            l.Content = tb;
            l.Foreground = Brushes.LightGray;
            l.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Grid.SetRow(l, rowIdx);
            Grid.SetColumn(l, 1);
            l.MouseDown += lblFolder_MouseDown;
            f.uiObjects.Add(l);
            f.physical = tb;

            Button but = new Button();
            but.Width = 16;
            but.Height = 16;
            but.ToolTip = "Synchronize solution folder to filesystem folder.";
            but.Background = getResourceImage("Refresh_16x16.png");
            but.Click += btnSynchronizeRow_Click;
            Grid.SetRow(but, rowIdx);
            Grid.SetColumn(but, 2);
            f.uiObjects.Add(but);

            but = new Button();
            but.Width = 16;
            but.Height = 16;
            but.ToolTip = "Remove this folder entry.";
            but.Background = getResourceImage("Remove_16x16.png");
            but.Click += btnRemoveRow_Click;
            Grid.SetRow(but, rowIdx);
            Grid.SetColumn(but, 3);
            f.uiObjects.Add(but);

            but = new Button();
            but.Width = 16;
            but.Height = 16;
            but.ToolTip = "Edit this folder entry.";
            but.Background = getResourceImage("Edit_16x16.png");
            but.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            but.Click += btnEditRow_Click;
            Grid.SetRow(but, rowIdx);
            Grid.SetColumn(but, 4);
            f.uiObjects.Add(but);

            f.token = token;

            folderRows.Add(f);
            SynchronizeLineBackgrounds();

            foreach (FrameworkElement elem in f.uiObjects)
            {
                elem.Tag = f.token;
                FoldersGrid.Children.Add(elem);
            }

            SynchronizeStatus();
        }

        void btnSynchronizeRow_Click(object sender_, RoutedEventArgs e)
        {
            FrameworkElement sender = (FrameworkElement)sender_;
            doSynchronizeRow(getRowIndexForToken((uint)sender.Tag));
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            if (!editFolderWindow.BringUp())
            {
                statusBar.SetText("");
                return;
            }
            uint token = folderStorage.Add();
            doAddRow(token, editFolderWindow.entry);
            folderStorage.Update(token, editFolderWindow.entry);
            statusBar.SetText("Created entry for physical solution folder " +
                editFolderWindow.entry.solutionFolder);
        }

        private void SynchronizeStatus()
        {
            if (!dte.Solution.IsOpen)
            {
                Status.Content = "No solution open.";
            }
            else if (folderRows.Count == 0)
            {
                Status.Content = "No solution folders.";
            }
            else
            {
                Status.Content = folderRows.Count.ToString() + " solution folders.";
            }
        }

        private void SynchronizeLineBackgrounds()
        {
            int numRows = folderRows.Count;
            int numBg = lineBackgrounds.Count;

            if (numBg < numRows)
            {
                for (; numBg < numRows; numBg++)
                {
                    Border b = new Border();
                    Color col = new Color();
                    if (numBg % 2 == 0)
                    {
                        col.R = 32;
                        col.G = 32;
                        col.B = 32;
                        col.A = 255;
                    }
                    else
                    {
                        col.R = 64;
                        col.G = 64;
                        col.B = 64;
                        col.A = 255;
                    }
                    b.Background = new SolidColorBrush(col);
                    Grid.SetRow(b, numBg + 1);
                    Grid.SetColumn(b, 0);
                    Grid.SetColumnSpan(b, numCols);
                    lineBackgrounds.Add(b);
                    FoldersGrid.Children.Add(b);
                }
            }
            else if (numBg > numRows)
            {
                for (; numBg > numRows; numBg--)
                {
                    Border b = lineBackgrounds[lineBackgrounds.Count - 1];
                    FoldersGrid.Children.Remove(b);
                    lineBackgrounds.RemoveAt(lineBackgrounds.Count - 1);
                }
            }

            for (int i = 0; i < numBg; i++)
            {
                Grid.SetRow(lineBackgrounds[i], i + 1);
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposeManagedAndNative)
        {
            if (disposeManagedAndNative)
                editFolderWindow.Dispose();
        }

        #region EmptyImplementationEvents
        public int OnBeforeCloseSolution(object pUnkReserved)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterLoadProject(IVsHierarchy pStubHierarchy, IVsHierarchy pRealHierarchy)
        {
            return VSConstants.S_OK;
        }

        public int OnAfterOpenProject(IVsHierarchy pHierarchy, int fAdded)
        {
            return VSConstants.S_OK;
        }

        public int OnBeforeCloseProject(IVsHierarchy pHierarchy, int fRemoved)
        {
            return VSConstants.S_OK;
        }

        public int OnBeforeUnloadProject(IVsHierarchy pRealHierarchy, IVsHierarchy pStubHierarchy)
        {
            return VSConstants.S_OK;
        }

        public int OnQueryCloseProject(IVsHierarchy pHierarchy, int fRemoving, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        public int OnQueryCloseSolution(object pUnkReserved, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        public int OnQueryUnloadProject(IVsHierarchy pRealHierarchy, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }
        #endregion
    }
}