﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.OLE;
using Microsoft.VisualStudio.Shell;
using EnvDTE80;

namespace LastShotStudiosInc.PhysicalSolutionFolders
{
    public partial class PhysicalSolutionFolders
    {
        public static String GetSolutionDirectory(DTE2 dte)
        {
            String s = System.IO.Path.GetDirectoryName(dte.Solution.FullName);
            if (s[s.Length - 1] != '\\') s = s + '\\';
            return s;
        }

        public static DTE2 GetDTE()
        {
            return (EnvDTE80.DTE2)Package.GetGlobalService(typeof(EnvDTE.DTE));
        }
    }
}
