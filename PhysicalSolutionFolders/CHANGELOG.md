﻿### Changelog

Version 3.0:

* Dropped explicit support for all versions of Visual Studio below 16.4.2 (16.4.29613)  
  NOTE: 16.4.2 is required for the bug-fix that Microsoft released that was crashing this extension.  
  SEE: https://developercommunity.visualstudio.com/content/problem/475435/e-invalidarg-error-when-removing-solution-folders.html

Version 2.0:

* Dropped explicit support for all versions of Visual Studio below 15.0

Version 1.12:

* Updated to Visual Studio Community 2015

Version 1.11:

* Nolonger destroys entire subtree on synchronize. Now updates selectively. Huge performance increase for update syncs.
* Added progress bar indicator.
* Better indicated VisualGit's licensing in LICENSE.txt
