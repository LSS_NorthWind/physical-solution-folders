﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.ComponentModel.Design;
using Microsoft.Win32;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.Shell;
using VisualGit.VSPackage.Attributes;

namespace LastShotStudiosInc.PhysicalSolutionFolders
{
    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    ///
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the 
    /// IVsPackage interface and uses the registration attributes defined in the framework to 
    /// register itself and its components with the shell.
    /// </summary>
    // This attribute tells the PkgDef creation utility (CreatePkgDef.exe) that this class is
    // a package.
    [PackageRegistration(UseManagedResourcesOnly = true)]
    // This attribute is used to register the information needed to show this package
    // in the Help/About dialog of Visual Studio.
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    // This attribute is needed to let the shell know that this package exposes some menus.
    [ProvideMenuResource("Menus.ctmenu", 1)]
    // This attribute registers a tool window exposed by this package.
    [ProvideToolWindow(typeof(MyToolWindow))]
    [Guid(GuidList.guidPhysicalSolutionFoldersPkgString)]
    [ProvideSolutionProperties(PhysicalSolutionFoldersPackage.KeyPhysicalSolutionFoldersSolution)]
    public sealed class PhysicalSolutionFoldersPackage :
        Package, IVsPersistSolutionProps, IFolderStorage
    {
        private const String KeyPhysicalSolutionFoldersSolution = "PhysicalSolutionFolders";
        private const String KeyNumEntries = "NumEntries";
        private const String KeySolutionFolder = "SolutionFolder";
        private const String KeyPhysicalFolder = "PhysicalFolder";
        private const string KeyFilter = "Filter";
        private const string KeyDirFilter = "DirFilter";
        private const String KeyRelative = "Relative";
        private const String KeyRecursive = "Recursive";
        private const String KeyForce = "Force";
        private const String KeyDropEmpty = "DropEmpty";

        struct TokenFolderStorageEntry
        {
            public uint token;
            public FolderStorageEntry entry;
        }
        private List<TokenFolderStorageEntry> folderStorageEntries;
        private Dictionary<uint, int> tokenToIndex;
        private uint nextToken;
        private bool dirtyProps;


        /// <summary>
        /// Default constructor of the package.
        /// Inside this method you can place any initialization code that does not require 
        /// any Visual Studio service because at this point the package object is created but 
        /// not sited yet inside Visual Studio environment. The place to do all the other 
        /// initialization is the Initialize method.
        /// </summary>
        public PhysicalSolutionFoldersPackage()
        {
            Debug.WriteLine(string.Format(CultureInfo.CurrentCulture, "Entering constructor for: {0}", this.ToString()));
            folderStorageEntries = new List<TokenFolderStorageEntry>();
            tokenToIndex = new Dictionary<uint, int>();
            nextToken = 0;
            dirtyProps = false;
        }

        /// <summary>
        /// This function is called when the user clicks the menu item that shows the 
        /// tool window. See the Initialize method to see how the menu item is associated to 
        /// this function using the OleMenuCommandService service and the MenuCommand class.
        /// </summary>
        private void ShowToolWindow(object sender, EventArgs e)
        {
            // Get the instance number 0 of this tool window. This window is single instance so this instance
            // is actually the only one.
            // The last flag is set to true so that if the tool window does not exists it will be created.
            MyToolWindow window = (MyToolWindow)this.FindToolWindow(typeof(MyToolWindow), 0, true);
            if ((null == window) || (null == window.Frame))
            {
                throw new NotSupportedException(Resources.CanNotCreateWindow);
            }
            IVsWindowFrame windowFrame = (IVsWindowFrame)window.Frame;
            Microsoft.VisualStudio.ErrorHandler.ThrowOnFailure(windowFrame.Show());
        }


        /////////////////////////////////////////////////////////////////////////////
        // Overridden Package Implementation
        #region Package Members

        /// <summary>
        /// Initialization of the package; this method is called right after the package is sited, so this is the place
        /// where you can put all the initialization code that rely on services provided by VisualStudio.
        /// </summary>
        protected override void Initialize()
        {
            Debug.WriteLine (string.Format(CultureInfo.CurrentCulture, "Entering Initialize() of: {0}", this.ToString()));
            base.Initialize();

            // Add our command handlers for menu (commands must exist in the .vsct file)
            OleMenuCommandService mcs = GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if ( null != mcs )
            {
                // Create the command for the tool window
                CommandID toolwndCommandID = new CommandID(GuidList.guidPhysicalSolutionFoldersCmdSet, (int)PkgCmdIDList.cmdidPhysicalSolutionFolders);
                MenuCommand menuToolWin = new MenuCommand(ShowToolWindow, toolwndCommandID);
                mcs.AddCommand( menuToolWin );
            }

            // Create window here to reliably set its folder storage
            MyToolWindow window = (MyToolWindow)this.FindToolWindow(typeof(MyToolWindow), 0, true);
            window.FolderStorage = this;
        }
        #endregion

        public int QuerySaveSolutionProps(IVsHierarchy pHierarchy, VSQUERYSAVESLNPROPS[] pqsspSave)
        {
            if (pqsspSave == null)
                return VSConstants.E_POINTER;

            // We only want to save to global section
            if (pHierarchy == null)
            {
                if (dirtyProps)
                    pqsspSave[0] = VSQUERYSAVESLNPROPS.QSP_HasDirtyProps;
                else
                    pqsspSave[0] = VSQUERYSAVESLNPROPS.QSP_HasNoDirtyProps;
                dirtyProps = false;
            }

            return VSConstants.S_OK;
        }

        public int SaveSolutionProps(IVsHierarchy pHierarchy, IVsSolutionPersistence pPersistence)
        {
            if (pPersistence == null)
                return VSConstants.E_POINTER;

            pPersistence.SavePackageSolutionProps(
                0, pHierarchy, this, KeyPhysicalSolutionFoldersSolution);

            return VSConstants.S_OK;
        }

        public int WriteSolutionProps(IVsHierarchy pHierarchy, string pszKey, IPropertyBag pPropBag)
        {
            if (pPropBag == null)
                return VSConstants.E_POINTER;

            pPropBag.Write(KeyNumEntries, folderStorageEntries.Count.ToString());
            for (int i = 0, end = folderStorageEntries.Count; i < end; i++)
            {
                String idxString = i.ToString();

                FolderStorageEntry entry = folderStorageEntries[i].entry;
                pPropBag.Write(idxString + KeySolutionFolder, entry.solutionFolder);
                pPropBag.Write(idxString + KeyPhysicalFolder, entry.physicalFolder);
                pPropBag.Write(idxString + KeyFilter, entry.filter);
                pPropBag.Write(idxString + KeyDirFilter, entry.dirFilter);
                pPropBag.Write(idxString + KeyRelative, entry.relative.ToString());
                pPropBag.Write(idxString + KeyRecursive, entry.recursive.ToString());
                pPropBag.Write(idxString + KeyForce, entry.force.ToString());
                pPropBag.Write(idxString + KeyDropEmpty, entry.dropEmpty.ToString());
            }

            return VSConstants.S_OK;
        }

        private static T ReadProp<T>(IPropertyBag bag, String key)
        {
            object val;
            bag.Read(key, out val, null, 0, null);
            if (val == null)
            {
                throw new PropertyReadConvertException(
                    "Failed to convert property with key " +
                    key +
                    " to type " +
                    typeof(T).ToString());
            }

            return (T)Convert.ChangeType((String)val, typeof(T));
        }

        public int ReadSolutionProps(IVsHierarchy pHierarchy, string pszProjectName, string pszProjectMk, string pszKey, int fPreLoad, IPropertyBag pPropBag)
        {
            try
            {
                int numEntries = ReadProp<int>(pPropBag, KeyNumEntries);
                List<FolderStorageEntry> temp = new List<FolderStorageEntry>();
                for (int i = 0; i < numEntries; i++)
                {
                    String iStr = i.ToString();

                    FolderStorageEntry entry = new FolderStorageEntry();
                    entry.solutionFolder = ReadProp<String>(
                        pPropBag, iStr + KeySolutionFolder);
                    entry.physicalFolder = ReadProp<String>(
                        pPropBag, iStr + KeyPhysicalFolder);
                    entry.filter = ReadProp<String>(
                        pPropBag, iStr + KeyFilter);
                    entry.dirFilter = ReadProp<String>(
                        pPropBag, iStr + KeyDirFilter);
                    entry.relative = ReadProp<bool>(
                        pPropBag, iStr + KeyRelative);
                    entry.recursive = ReadProp<bool>(
                        pPropBag, iStr + KeyRecursive);
                    entry.force = ReadProp<bool>(
                        pPropBag, iStr + KeyForce);
                    entry.dropEmpty = ReadProp<bool>(
                        pPropBag, iStr + KeyDropEmpty);
                    temp.Add(entry);
                }

                // If got here, nothing is corrupt, safe to replace our data
                folderStorageEntries.Clear();
                tokenToIndex.Clear();
                nextToken = 0;

                IFolderStorage storage = this;
                foreach (FolderStorageEntry entry in temp)
                {
                    uint token = storage.Add();
                    storage.Update(token, entry);
                }
                dirtyProps = false;

                // Notify clients
                if (AfterReloaded != null)
                    AfterReloaded(this, null);
            }
            catch (Exception)
            {
            }

            return VSConstants.S_OK;
        }

        uint IFolderStorage.Add()
        {
            TokenFolderStorageEntry entry = new TokenFolderStorageEntry();
            entry.token = nextToken++;
            folderStorageEntries.Add(entry);
            tokenToIndex.Add(entry.token, folderStorageEntries.Count - 1);

            dirtyProps = true;

            return entry.token;
        }

        void IFolderStorage.Remove(uint token)
        {
            if (!tokenToIndex.ContainsKey(token))
                throw new KeyNotFoundException();

            folderStorageEntries.RemoveAt(tokenToIndex[token]);
            tokenToIndex.Remove(token);
            dirtyProps = true;
        }

        void IFolderStorage.Update(uint token, FolderStorageEntry entry)
        {
            if (!tokenToIndex.ContainsKey(token))
                throw new KeyNotFoundException();

            TokenFolderStorageEntry tokenEntry =
                folderStorageEntries[tokenToIndex[token]];
            tokenEntry.entry = entry;
            folderStorageEntries[tokenToIndex[token]] = tokenEntry;
            dirtyProps = true;
        }

        FolderStorageEntry IFolderStorage.Get(uint token)
        {
            if (!tokenToIndex.ContainsKey(token))
                throw new KeyNotFoundException();

            return folderStorageEntries[tokenToIndex[token]].entry;
        }

        int IFolderStorage.Count
        {
            get
            {
                return folderStorageEntries.Count;
            }
        }

        uint IFolderStorage.GetToken(int index)
        {
            if (index < 0 || index > folderStorageEntries.Count)
                throw new IndexOutOfRangeException();

            return folderStorageEntries[index].token;
        }

        public event FolderStorageReloadedHandler AfterReloaded;

        #region EmptyImplementationEvents
        public int LoadUserOptions(IVsSolutionPersistence pPersistence, uint grfLoadOpts)
        {
            return VSConstants.S_OK;
        }

        public int OnProjectLoadFailure(IVsHierarchy pStubHierarchy, string pszProjectName, string pszProjectMk, string pszKey)
        {
            return VSConstants.S_OK;
        }

        public int ReadUserOptions(IStream pOptionsStream, string pszKey)
        {
            return VSConstants.S_OK;
        }

        public int SaveUserOptions(IVsSolutionPersistence pPersistence)
        {
            return VSConstants.S_OK;
        }

        public int WriteUserOptions(IStream pOptionsStream, string pszKey)
        {
            return VSConstants.S_OK;
        }
        #endregion
    }
}
