﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace LastShotStudiosInc.PhysicalSolutionFolders
{

    public delegate void FolderStorageReloadedHandler(object sender, EventArgs e);

    /* Summary:
     *      A single synchronization entry representing a link between a virtual solution
     *      folder and a physical folder on the filesystem.
     *
     * Members:
     *      solutionFolder:
     *          Virtual path to folder in the solution in the form a/b/c...
     *
     *      physicalFolder:
     *          Path to folder on the filesystem. Can be relative to the
     *          solution file or absolutte, depending on the value of relative.
     *
     *      relative:
     *          Whether the physicalFolder path is relative to the location of
     *          the solution file or if it is absolute on the filesystem.
     */
    public struct FolderStorageEntry
    {
        public String solutionFolder;
        public String physicalFolder;
        public String filter;
        public String dirFilter;
        public bool relative;
        public bool recursive;
        public bool force;
        public bool dropEmpty;

        // Detect if default to let client decide whether to initialize or not
        public bool IsDefault()
        {
            return String.IsNullOrEmpty(solutionFolder) &&
                String.IsNullOrEmpty(physicalFolder) &&
                String.IsNullOrEmpty(filter) &&
                String.IsNullOrEmpty(dirFilter) &&
                !relative &&
                !recursive &&
                !force &&
                !dropEmpty;
        }

        // Since can't have default ctor, this is a nasty workaround...
        public void Initialize()
        {
            solutionFolder = "";
            physicalFolder = "";
            filter = ".*";
            dirFilter = ".*";
            relative = true;
            recursive = true;
            force = false;
            dropEmpty = true;
        }

        public String GetPhysicalFolderAbsolute(EnvDTE80.DTE2 dte)
        {
            String p = physicalFolder;
            if (relative) 
                p = System.IO.Path.Combine(
                    PhysicalSolutionFolders.GetSolutionDirectory(
                        PhysicalSolutionFolders.GetDTE()),
                    p);
            p = System.IO.Path.GetFullPath(p);
            return p;
        }
    }

    /* Summary:
     *      Provides a mechanism for adding, removing, and updating folder entries.
     */
    public interface IFolderStorage
    {

        /* Summary:
         *      Add a new entry.
         * 
         * Returns:
         *      Token that could be used to read the entry or write to the entry.
         */
        uint Add();

        /* Summary:
         *      Erases the entry identified by the token.
         */
        void Remove(uint token);

        /* Summary:
         *      Overwrites the entry at the provided token to the given entry.
         */
        void Update(uint token, FolderStorageEntry entry);

        /* Summary:
         *      Retrieves the entry at the provided token.
         *
         * Returns:
         *      Entry at the given token.
         */
        FolderStorageEntry Get(uint token);

        /* Summary:
         *      Number of entries currently stored.
         */
        int Count
        {
            get;
        }

        /* Summary:
         *      Given an entry index, returns a token identifying that entry.
         *      Used after entries are updated in the background.
         *
         * Parameters:
         *      index:
         *          Index of the desired entry.
         *
         * Returns:
         *      Token representing the indexed entry.
         */
        uint GetToken(int index);

        /* Summary:
         *      
         */
        event FolderStorageReloadedHandler AfterReloaded;
    }
}
