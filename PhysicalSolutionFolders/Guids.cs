﻿// Guids.cs
// MUST match guids.h
using System;

namespace LastShotStudiosInc.PhysicalSolutionFolders
{
    static class GuidList
    {
        public const string guidPhysicalSolutionFoldersPkgString = "e76700eb-2a5c-4b63-948f-3fc96127b1b1";
        public const string guidPhysicalSolutionFoldersCmdSetString = "5b888eb8-c73b-4460-bf64-d7ad0d2d34ce";
        public const string guidToolWindowPersistanceString = "443b0d85-f0f6-4a51-a4ee-95e20f159ea0";

        public static readonly Guid guidPhysicalSolutionFoldersCmdSet = new Guid(guidPhysicalSolutionFoldersCmdSetString);
    };
}