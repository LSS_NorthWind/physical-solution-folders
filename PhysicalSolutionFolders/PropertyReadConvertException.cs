﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LastShotStudiosInc.PhysicalSolutionFolders
{
    [Serializable]
    public class PropertyReadConvertException : Exception
    {
        public PropertyReadConvertException(string message = "") :
            base(message)
        {
        }
    }
}
